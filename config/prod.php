<?php

// configure your app for the production environment

$app['twig.path'] = array(__DIR__.'/../templates');
$app['twig.options'] = array('cache' => __DIR__.'/../var/cache/twig');

$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
    'db.options' => array(
        'driver'   => 'pdo_mysql',
        'host' => 'localhost',
        'dbname' => 'urbaqrce_payments',
        'user' => 'urbaqrce_silex',
        'password' => 'n7qWXEP{RkuG',
    ),
));

// TPV Virtual BSabadell
// update key parameter to use it in live enviroment
$app['redsys'] = array(
	'owner' => 'Martocchi Group SL',
	'trade' => 'Ghosts In Barcelona',
	'key' => 'nzALwaoPmMAzsCwelXn4xDIu7GQOcGhC',
	'fuc' => '061267753',
	'env' => 'live',
    'url_ok' => 'http://queglobo.com/%1/checkout/%2/ok',
    'url_ko' => 'http://queglobo.com/%1/checkout/%1',
    'url_notification' => 'http://queglobo.com/checkout/notification',
);

$app['check_mobile'] = array(
    'key' => '8186532741f1c6d904753c442d5f7b63',
    'url' => 'http://apilayer.net/api/validate'
);

// Set local time
date_default_timezone_set("Europe/Madrid");
