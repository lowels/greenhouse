<?php

use Silex\Provider\MonologServiceProvider;
use Silex\Provider\WebProfilerServiceProvider;

// include the prod configuration
require __DIR__.'/prod.php';

// enable the debug mode
$app['debug'] = true;

$app->register(new MonologServiceProvider(), array(
    'monolog.logfile' => __DIR__.'/../var/logs/silex_dev.log',
));

$app->register(new WebProfilerServiceProvider(), array(
    'profiler.cache_dir' => __DIR__.'/../var/cache/profiler',
));

$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
    'db.options' => array(
        'driver'   => 'pdo_mysql',
        'host' => 'localhost',
        'dbname' => 'martocchi_payments',
        'user' => 'root',
        'password' => 'root',
    ),
));

// TPV Virtual BSabadell
$app['redsys'] = array(
	'owner' => 'Martocchi Group SL',
	'trade' => 'Ghosts In Barcelona',
	'key' => 'sq7HjrUOBfKmC576ILgskD5srU870gJ7',
	'fuc' => '061267753',
	'env' => 'test',
    'url_ok' => 'http://queglobo.com/web/%1/checkout/%2/ok',
    'url_ko' => 'http://queglobo.com/web/%1',
    'url_notification' => 'http://queglobo.com/web/checkout/notification',
);
