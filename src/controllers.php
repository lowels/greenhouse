<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

//Request::setTrustedProxies(array('127.0.0.1'));

/*
 *  Home Page
 */
$app->get('/', function () use($app) {
    return $app['twig']->render('index.html.twig', array('open' => isOpen($app), 'clubs' => getAllClubs($app)));    
})->bind('homepage');

/*
 *  Shopping page by club
 */
$app->get('/{club}', function ($club) use ($app) {
    if (isOpen($app) && clubExist($club, $app)) {
        if (!$app['session']->has('order_id')) {
            $app['session']->set('order_id', strtoupper(substr(md5(microtime()),rand(0,26),4) . date('d')));
        }

        return $app['twig']->render('shop.html.twig', array(
            'products' => getProductList($app), 
            'lines' => getAllCartLines($app['session']->get('order_id'), $app),
            'total' => getTotal($app['session']->get('order_id'), $app),
            'items' => getTotalItems($app['session']->get('order_id'), $app),
            'club' => getClubInfo($club, $app),
        ));
    }
    return $app->redirect($app['url_generator']->generate('homepage'));
})->bind('shop');

/*
 *  Add new product to the shopiing cart
 */
$app->post('/add', function(Request $request) use ($app) {
    $response = array('result' => 'success', 'message' => '% añadido correctamente', 'lines' => array());
    $product = addNewOrderLine($request, $app);
    if($product) {
        $response['message'] = str_replace('%', $product['nombre'], $response['message']);
    }
    else {
        $response['result'] = 'error';
        $response['message'] = 'Glups! Algo falló';
    }

    $response['lines'] = getAllCartLines($request->request->get('order'), $app);
    $response['total'] = getTotal($request->request->get('order'), $app);
    $response['items'] = getTotalItems($request->request->get('order'), $app);
    return new Response(json_encode($response), 200);
})->bind('add_product');

/*
 *  Remove product from the shopping cart
 */
$app->post('/delete', function(Request $request) use($app) {
    $response = array('result' => 'success', 'message' => '% eliminado correctamente', 'lines' => array());
    $product = deleteOrderLine($request->request->get('line'), $app);

    if($product) {
        $response['message'] = str_replace('%', $product['nombre'], $response['message']);
    }
    else {
        $response['result'] = 'error';
        $response['message'] = 'Glups! Algo falló';
    }

    $response['lines'] = getAllCartLines($product['line']['order_id'], $app);
    $response['total'] = getTotal($product['line']['order_id'], $app);
    $response['items'] = getTotalItems($product['line']['order_id'], $app);
    return new Response(json_encode($response), 200);
})->bind('delete_line');

/*
 *  Checkout process
 */
$app->post('/checkout', function(Request $request) use($app) {
    $response = array('result' => 'error', 'message' => 'Datos incorrectos', 'errors' => array());
    $response['errors'] = checkInputData($request, $app);
    $order_id = $request->request->get('order_id');

    if (count($response['errors']) == 0 && getTotal($order_id, $app) > 0) {    
        try{
            $redsys = new Sermepa\Tpv\Tpv();
            $redsys->setAmount(getTotal($order_id, $app));
            $redsys->setOrder(rand(1000,9999).'-'.$order_id);
            $redsys->setMerchantcode($app['redsys']['fuc']);
            $redsys->setCurrency('978');
            $redsys->setTransactiontype('0');
            $redsys->setTerminal('1');
            $redsys->setMethod('C');

            // Some workaround, until find the way to get ABS URL
            $redsys->setNotification($app['redsys']['url_notification']);
            $redsys->setUrlOk(str_replace(array('%1', '%2'), array($request->request->get('club'), $order_id), $app['redsys']['url_ok']));
            $redsys->setUrlKo(str_replace(array('%1'), array($request->request->get('club')), $app['redsys']['url_ko']));
            //$redsys->setUrlOk($app['url_generator']->generate('checkout_ok', array('club' => $request->request->get('club'), 'order_id' => $order_id), UrlGeneratorInterface::ABSOLUTE_URL));
            //$redsys->setUrlKo($app['url_generator']->generate('shop', array('club' => $request->request->get('club')), UrlGeneratorInterface::ABSOLUTE_URL));
            $redsys->setVersion('HMAC_SHA256_V1');
            $redsys->setTradeName($app['redsys']['trade']);
            $redsys->setTitular($app['redsys']['owner']);
            $redsys->setProductDescription(getCheckoutDescription($order_id, $app));
            $redsys->setEnvironment($app['redsys']['env']);
            $redsys->setAttributesSubmit('btn_submit', 'btn_id', 'Enviar', 'display:none;');
            $redsys->setMerchantData(createOrder($request, $app));
            $signature = $redsys->generateMerchantSignature($app['redsys']['key']);
            $redsys->setMerchantSignature($signature);

        } catch (\Sermepa\Tpv\TpvException $e) {
            echo $e->getMessage();
        }
        
        $response['result'] = 'success';
        $response['message'] = $redsys->createForm();        
    }
    return new Response(json_encode($response), 200);
})->bind('checkout');

/*
 *  Checkout response, called by Payment Gateway
 */
$app->get('/{club}/checkout/{order_id}/ok', function($club, $order_id) use($app) {
    if ($app['session']->has('order_id')) {
        $app['session']->remove('order_id');
    }

    return $app['twig']->render('checkout_ok.html.twig', array(
        'lines' => getAllCartLines($order_id, $app),
        'total' => getTotal($order_id, $app),
        'club' => getClubInfo($club, $app),
        'order' => $order_id,
    ));
})->bind('checkout_ok');

/*
 *  Checkout notification response, called by Payment Gateway
 */
$app->post('/checkout/notification', function(Request $request) use($app) {
    try{
        $redsys = new Sermepa\Tpv\Tpv();
        $parameters = $redsys->getMerchantParameters($_POST["Ds_MerchantParameters"]);
        $DsResponse = $parameters["Ds_Response"];
        $DsResponse += 0;
        if ($redsys->check($app['redsys']['key'], $_POST) && $DsResponse <= 99) {
            // Save order
            updateOrderStatus($parameters['Ds_MerchantData'], $app, 2);
            addOrderResponse($parameters['Ds_MerchantData'], $_POST["Ds_MerchantParameters"], $app);
            // TODO: send sms/mail
        } else {
            // Something went wrong
            updateOrderStatus($parameters['Ds_MerchantData'], $app, 6);
        }
    } catch (\Sermepa\Tpv\TpvException $e) {
        echo $e->getMessage();
    }
})->bind('payment_notification');

/*
 *  Error Handling
 */
$app->error(function (\Exception $e, Request $request, $code) use ($app) {
    if ($app['debug']) {
        return;
    }

    // 404.html, or 40x.html, or 4xx.html, or error.html
    $templates = array(
        'errors/'.$code.'.html.twig',
        'errors/'.substr($code, 0, 2).'x.html.twig',
        'errors/'.substr($code, 0, 1).'xx.html.twig',
        'errors/default.html.twig',
    );

    return new Response($app['twig']->resolveTemplate($templates)->render(array('code' => $code)), $code);
});

/*
 *  Get all products in the shopping cart for the checkout description
 */
function getCheckoutDescription($order_id, $app){
    $lines = getAllCartLines($order_id, $app);
    $description = '';
    foreach ($lines as $key => $line) {
        $description .= $line['quantity'] . 'x '. $line['nombre'] . PHP_EOL;
    }
    return $description;
}

/*
 *  Get all products available
 */
function getProductList($app) {
    $aux = array(2 => 'pizza', 3 => 'pasta', 5 => 'sandwich');
    $products = array();
    $db_products = $app['db']->fetchAll("SELECT * FROM glovo_products WHERE available = 1 ORDER BY greenhouse_price DESC, nombre ASC");
    foreach ($db_products as $key => $product) {
        if (array_key_exists($product['tipo'], $aux)) {
            $products[$aux[$product['tipo']]][] = $product;
        }
        elseif (in_array($product['id'], array(2,3,46))) {
            $products['starter'][] = $product;
        }        
    }
    return $products;
}

/*
 *  Compute total amount shopping cart
 */
function getTotal($order_id, $app) {
    $total = $app['db']->fetchAssoc("SELECT sum(total) AS total FROM cart_lines WHERE order_id LIKE ?", array($order_id));
    if (is_null($total['total'])) {
        $total = 0;
    }
    else {
        $total = $total['total'];
    }
    return number_format($total, 2);
}

/*
 *  Compute total elements in shopping cart
 */
function getTotalItems($order_id, $app) {
    $total = $app['db']->fetchAssoc("SELECT sum(quantity) AS items FROM cart_lines WHERE order_id LIKE ?", array($order_id));
    if (is_null($total['items'])) {
        $total = 0;
    }
    else {
        $total = $total['items'];
    }
    return $total;
}

/*
 *  Delete one line from the shopping cart
 */
function deleteOrderLine($line, $app) {
    $line = $app['db']->fetchAssoc("SELECT * FROM cart_lines WHERE id = ?", array((int)$line));
    $product = $app['db']->fetchAssoc("SELECT * FROM glovo_products WHERE id = ?", array((int)$line['product_id']));
    $product['line'] = $line;
    $app['db']->delete('cart_lines', array('id' => (int)$line['id']));
    return $product;
}

/*
 *  Insert one new line, update it if exist
 */
function addNewOrderLine($request, $app) {
    $product = $app['db']->fetchAssoc("SELECT * FROM glovo_products WHERE id = ?", array((int)$request->request->get('product')));
    $line = $app['db']->fetchAssoc("SELECT * FROM cart_lines WHERE order_id LIKE ? AND product_id = ?", array($request->request->get('order'), (int)$request->request->get('product')));
    if ($line) {
        $app['db']->update('cart_lines', array(
            'quantity' => $line['quantity'] + 1,
            'total' => $line['total'] + $line['price']
        ), 
        array('id' => $line['id']));
    } else {
        $app['db']->insert('cart_lines', array(
            'order_id' => $request->request->get('order'),
            'product_id' => $product['id'],
            'price' => $product['greenhouse_price'],
            'quantity' => 1,
            'total' => $product['greenhouse_price']
        ));
    }
    return $product;
}

/*
 *  Get all shopping cart lines
 */
function getAllCartLines($order_id, $app) {
    return $app['db']->fetchAll("SELECT *, cart_lines.id AS line_id FROM cart_lines LEFT JOIN glovo_products ON cart_lines.product_id = glovo_products.id WHERE order_id LIKE ?", array($order_id));
}

/*
 *  Check if a given club exist in DB
 */
function clubExist($club, $app) {
    return $app['db']->fetchAssoc("SELECT * FROM greenhouse WHERE available = 1 AND slug LIKE ?", array($club));
}

/*
 *  Check that checkout data is correct
 */
function checkInputData($request, $app) {
    $errors = array();
    if (!$request->request->has('legal')) {
        $errors['legal'] = 'Debes aceptar las condicones';
    }
    if (empty($request->request->get('member-id'))) {
        $errors['member-id'] = 'Introduze tu n&uacute;mero de socio';
    } 
    if (empty($request->request->get('phone'))) {
        $errors['phone'] = 'Introduze tu n&uacute;mero de tel&eacute;fono';
    }
    if (strlen($request->request->get('phone')) < 7 or !checkPhone($request->request->get('phone'), $app)) {
        $errors['phone'] = 'Tel&eacute;fono no v&aacute;lido';
    }
    if (!clubExist($request->request->get('club'), $app)) {
        $errors['general'] = 'Club no permitido';
    }
    if (getTotal($request->request->get('order_id'), $app) <= 0) {
        $errors['general'] = '&iexcl;No hay productos!';
    }


    return $errors;
}

/*
 *  Create new order
 */
function createOrder($request, $app) {
    $club = $app['db']->fetchAssoc("SELECT id FROM greenhouse WHERE slug LIKE ?", array($request->request->get('club')));
    $order = $app['db']->insert('orders', array(
        'order_id' => $request->request->get('order_id'),
        'greenhouse_id' => $club['id'],
        'member_id' => $request->request->get('member-id'),
        'phone' => $request->request->get('phone'),
        'total' => getTotal($request->request->get('order_id'), $app),
        'status_id' => 1,
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s'),
    ));

    $order = $app['db']->fetchAssoc("SELECT MAX(id) AS id FROM orders");
    return $order['id'];
}

/*
 *  Include response from payment gateway
 */
function addOrderResponse($order_id, $response, $app) {
    $app['db']->update('orders', array(
        'response' => $response,
    ), 
    array('id' => $order_id));
}

/*
 *  Update order status
 */
function updateOrderStatus($order_id, $app, $status_id=1) {
    $app['db']->update('orders', array(
        'status_id' => $status_id,
    ), 
    array('id' => $order_id));
}

/*
 *  Return club information
 */
function getClubInfo($club, $app) {
    return $app['db']->fetchAssoc("SELECT * FROM greenhouse WHERE slug LIKE ?", array($club));
}

/*
 *  Get a list of all clubs
 */
function getAllClubs($app) {
    return $app['db']->fetchAll("SELECT * FROM greenhouse WHERE available = 1");
}

/*
 *  Check if a given phone number exist in DB
 */
function phoneExist($phone, $app) {
    return $app['db']->fetchAssoc("SELECT * FROM orders WHERE phone LIKE ?", array($phone));
}

function checkPhone($phone, $app) {
    if (in_array($phone, array('620387357', '932505251')) || phoneExist($phone, $app)) {
        return true;
    }
    // set API Access Key
    $access_key = $app['check_mobile']['key'];

    // set phone number
    $phone_number = $phone;

    // Initialize CURL:
    $url = 'http://apilayer.net/api/validate?access_key='.$access_key.'&number='.$phone_number;

    // Check if Spanish
    if (substr($phone, 0, 2) != '00' && substr($phone, 0, 1) != '+') {
        $url .= '&country_code=ES';
    }
    
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    // Store the data:
    $json = curl_exec($ch);
    curl_close($ch);

    // Decode JSON response:
    $validationResult = json_decode($json, true);

    return $validationResult['valid'];
}

function isOpen($app){
    return $app['db']->fetchColumn("SELECT value FROM settings WHERE name LIKE 'shop_open'");
}